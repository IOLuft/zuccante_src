# UDP

Gli esempi che vengono proposti:
```
DaytimeUDPServer.java 
DaytimeUDPClient.java 
```
in cui il client invia datagrammi di richiesta senza dati e con la sola intestazione in modo che il server possa offrire un semplice servizio di data (in mancanza di un servizio in Internet che lo offra sotto protocollo UDP),

```
UDPClient.java
UDPServer.java
```
versione UDP di `CapitalizeServer` programma visti per TCP,
```
QuoteClient.java
QuoteServer.java
```
con un server multythread che serve più client ed infine
```
MulticastServer.java
MulticastClient.java
```
la versione multicast del server precedente.