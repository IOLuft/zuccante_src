# Java Basic

In questi sorgenti proponiamo una proposta di avvicinamento alla programmazione Java.

## Introduzione

I primi due programmi per il III anno di Informatica

[HelloWorld.java](./HelloWorld.java)  
[Somma.java](./Somma.java)

## terzo anno

Si prendano in considerazione gli esempi delle cartelle

[objects](./objects)

## quarto anno

Si prendano in considerazione gli esempi delle cartelle

[extending](./extending)


## materiali


[1] A.Morettin ["A Java Journey"](https://drive.google.com/open?id=1Z4PnYiKlmaGrRdt0y1YwKOeGWtEM_wLK) per la classe terza.  
[2] K.Arnold ... ["The Java Programming Language"](https://doc.lagout.org/programmation/Java/The%20Java%20Programming%20Language%20%284th%20ed.%29%20%5BArnold%2C%20Gosling%20%26%20Holmes%202005-08-27%5D.pdf).  
[3] A.Morettin ["appunti di Strutture Dati ... in fase beta"](https://drive.google.com/open?id=1KkpIsNk1xuwy-tXxcKwrpQk2IhyxIPWD) per la classe quarta.  
[4] R.C.Martin **"UML for Java programmers"** per il triennio.  
[5] J.Schmuller **"UML in 24 Hours"** per il triennio.
