public class Main {
    
    public static void main (String[] args) {
        
        Point p1 = new Point();
        Point p2 = new Point();
        
        System.out.println("l'ascissa di p1 è: " + p1.x);
        System.out.println("origin è il punto (" + Point.origin.x + ", " + Point.origin.y + ")");
        
        p1.x = 0.0d;
        p1.y = 3.0;
        
        p1.x = 4.0d;
        p1.y = 0.0d;
        
        System.out.println("la distanza fra p1 e p2 è: " + p1.distance(p2));
        
        
        
    }


}
