class Base {
    protected String name() {
        return "Base";
    }
}

class More extends Base {
    protected String name() {
        return "More";
    }
    
    protected String superName() {
        return super.name();
    }
}

public class Main001 {
    public static void main(String[] args){
        More more = new More();
        Base sref = (Base) more;
    
        System.out.println("this.name() = " + more.name());
        System.out.println("sref.name() = " + sref.name());
        System.out.println("super.name() = " + more.superName());
    }
}



