# Aggregate

Per l'illustrazione del **pipeline**: [qui](https://docs.mongodb.com/manual/aggregation/#aggregation-pipeline).  
Per un parallelo SQL MongoDB: [qui](https://docs.mongodb.com/manual/reference/sql-comparison/).  
Per gli **stage** del pipeline: [qui]().

Vediamo qui di seguito alcuni esempi interessanti

## Unjoin

Qui illustriamo una sorta di **reverse engineering**:
```
db.docenti.insert([
{nome: "Andrea", cognome: "Morettin", materie: [ "TPSIT", "Informatica"]},
{nome: "Carlo", cognome: "Salvagno", materie: [ "GPOI", "Informatica"]},
{nome: "Sergio", cognome: "Mattiello", materie: [ "Sistemi", "Informatica"]},
{nome: "Franco", cognome: "Zottinoo", materie: [ "Sistemi"]}
])

db.docenti.aggregate( [ { $unwind : "$materie" } ] )

db.docenti.aggregate( [ { $unwind : "$materie" } ], [ { $project : { _id: 0, materie : 1 } } )

db.docenti.aggregate( [ { $unwind : "$materie" }, { $project : { _id: 0, materie : 1 } } ] )

// change materie to nome
db.docenti.aggregate( [ { $unwind : "$materie" }, { $project : { _id: 0, nome : '$materie' } } ] )

db.docenti.aggregate( [ { $unwind : "$materie" }, { $project : { _id: 0, nome : "$materie" } }, {$out: "materie"} ] )

db.materie.find()
db.materie.find({})

db.materie.drop()

```

## Outer join

Naturalmente esiste pure per MongoDB l'**outer join**:
```
db.orders.insert([
   { "_id" : 1, "item" : "almonds", "price" : 12, "quantity" : 2 },
   { "_id" : 2, "item" : "pecans", "price" : 20, "quantity" : 1 },
   { "_id" : 3  }
])

db.inventory.insert([
   { "_id" : 1, "sku" : "almonds", description: "product 1", "instock" : 120 },
   { "_id" : 2, "sku" : "bread", description: "product 2", "instock" : 80 },
   { "_id" : 3, "sku" : "cashews", description: "product 3", "instock" : 60 },
   { "_id" : 4, "sku" : "pecans", description: "product 4", "instock" : 70 },
   { "_id" : 5, "sku": null, description: "Incomplete" },
   { "_id" : 6 }
])

db.orders.aggregate([
   {
     $lookup:
       {
         from: "inventory",
         localField: "item",
         foreignField: "sku",
         as: "inventory_docs"
       }
  }
])
```
